<?php

namespace Itomych\Pwinty\Api;

use stdClass;

abstract class AbstractBaseObject
{
    /**
     * @var ObjectManager
     */
    protected $objectManager;

    /**
     * AbstractBaseObject constructor.
     * @param ObjectManager $objectManager
     */
    public function __construct(ObjectManager $objectManager)
    {
        $this->objectManager = $objectManager;
    }

    abstract public function setData(stdClass $data);
}