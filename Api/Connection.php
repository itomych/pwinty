<?php

namespace Itomych\Pwinty\Api;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Psr\Http\Message\ResponseInterface;
use stdClass;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

class Connection
{
    protected const LIVE_PWINTY_URL = 'https://api.pwinty.com/v3.0/';
    protected const SANDBOX_PWINTY_URL = 'https://sandbox.pwinty.com/v3.0/';

    /**
     * @var bool - param is used sandbox mode for api requests
     */
    protected $sandboxMode;

    /**
     * @var string
     */
    protected $merchantId;

    /**
     * @var string
     */
    protected $apiKey;

    /**
     * @var Client
     */
    protected $api;

    /**
     * Connection constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $pwintyConfig = $container->getParameter('pwinty');

        $this->sandboxMode = $pwintyConfig['sandbox_mode'];
        $this->merchantId = $pwintyConfig['merchant_id'];
        $this->apiKey = $pwintyConfig['api_key'];

        $this->api = $this->initApiClient();
    }

    /**
     * @return string
     */
    protected function getApiUrl(): string
    {
        return $this->sandboxMode ? self::SANDBOX_PWINTY_URL : self::LIVE_PWINTY_URL;
    }

    /**
     * Init Guzzle client, set headers, base url
     * @return Client
     */
    protected function initApiClient(): Client
    {
        return new Client($this->getApiClientConfig());
    }

    /**
     * @return array
     */
    protected function getApiClientConfig(): array
    {
        return [
            'base_uri' => $this->getApiUrl(),
            'headers' => [
                'X-Pwinty-MerchantId' => $this->merchantId,
                'X-Pwinty-REST-API-Key' => $this->apiKey,
                'Content-type' => 'application/json',
                'accept' => 'application/json'
            ]
        ];
    }

    /**
     * @return Client
     */
    public function getApiClient()
    {
        return $this->api;
    }

    /**
     * @param string $method
     * @param string $url
     * @param null|array $data
     * @return ResponseInterface
     * @throws GuzzleException
     */
    public function request(string $method, string $url, ?array $data = null): ResponseInterface
    {
        if ($data === null) {
            return $this->api->request($method, $url);
        }

        if ($method === Request::METHOD_GET) {
            return $this->api->request($method, $url, ['query' => $data]);
        }

        return $this->api->request($method, $url, ['json' => $data]);
    }

    /**
     * @param string $url
     * @param null|array $data
     * @return stdClass
     * @throws GuzzleException
     */
    public function get(string $url, ?array $data = null): stdClass
    {
        $response = $this->request(Request::METHOD_GET, $url, $data);
        $data = json_decode($response->getBody()->getContents());

        return $data;
    }

    /**
     * @param string $url
     * @param null|array $data
     * @return stdClass
     * @throws GuzzleException
     */
    public function post(string $url, ?array $data = null): stdClass
    {
        $response = $this->request(Request::METHOD_POST, $url, $data);
        $data = json_decode($response->getBody()->getContents());

        return $data;
    }
}