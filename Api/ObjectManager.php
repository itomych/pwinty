<?php

namespace Itomych\Pwinty\Api;

use Doctrine\Common\Collections\ArrayCollection;
use stdClass;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Itomych\Pwinty\Api\AbstractBaseObject;

class ObjectManager
{

    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * ObjectManager constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * Map array objects to class, set object to class construct
     * @param array $data
     * @param string $class
     * @return ArrayCollection|AbstractBaseObject[]
     */
    public function mapToClass(array $data, string $class): ArrayCollection
    {
        $data = new ArrayCollection($data);

        return $data->map(function ($item) use ($class) {
            return $this->toClass($item, $class);
        });
    }

    /**
     * Create class and set data to object
     * @param stdClass $item
     * @param string $class
     * @return AbstractBaseObject
     */
    public function toClass(stdClass $item, string $class): AbstractBaseObject
    {
        /**
         * @var $object AbstractBaseObject
         */
        $object = $this->container->get($class);
        $object->setData($item);

        return $object;
    }

    /**
     * @param $class
     * @return AbstractBaseObject|object
     */
    public function createEmptyObject($class): AbstractBaseObject
    {
        return $this->container->get($class);
    }
}