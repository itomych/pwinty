<?php

namespace Itomych\Pwinty\Api;

abstract class AbstractService
{
    /**
     * @var ObjectManager
     */
    protected $objectManager;

    /**
     * @var Connection
     */
    protected $connection;

    /**
     * AbstractService constructor.
     * @param Connection $connection
     * @param ObjectManager $objectManager
     */
    public function __construct(Connection $connection, ObjectManager $objectManager)
    {
        $this->connection = $connection;
        $this->objectManager = $objectManager;
    }

    /**
     * Replace in url parts by params argument
     * @param string $url
     * @param array $params
     * @return string
     */
    protected function buildUrl(string $url, array $params)
    {
        foreach ($params as $name => $param) {
            $url = str_replace('{' . $name . '}', $param, $url);
        }

        return $url;
    }

}