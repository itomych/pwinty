<?php

namespace Itomych\Pwinty\Api;

use Itomych\Pwinty\Api\Connection;
use Itomych\Pwinty\Api\ObjectManager;
use Itomych\Pwinty\Api\Objects\Country;
use Itomych\Pwinty\Api\Objects\Image;
use Itomych\Pwinty\Api\Objects\Order;
use Itomych\Pwinty\Api\Objects\Product;
use Itomych\Pwinty\Api\Objects\ValidationOrder;
use Itomych\Pwinty\Api\Service\CountryService;
use Itomych\Pwinty\Api\Service\ImageService;
use Itomych\Pwinty\Api\Service\OrderService;
use Itomych\Pwinty\Api\Service\ProductService;
use Doctrine\Common\Collections\ArrayCollection;
use stdClass;

class PwintyApi
{

    /**
     * @var OrderService
     */
    public $order;

    /**
     * @var ImageService
     */
    public $image;

    /**
     * @var CountryService
     */
    public $country;

    /**
     * @var ProductService
     */
    public $product;


    /**
     * PwintyApi constructor.
     * @param OrderService $order
     * @param ImageService $image
     * @param CountryService $country
     * @param ProductService $product
     */
    public function __construct(
        OrderService $order,
        ImageService $image,
        CountryService $country,
        ProductService $product
    ) {
        $this->order = $order;
        $this->image = $image;
        $this->country = $country;
        $this->product = $product;
    }

}