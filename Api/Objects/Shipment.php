<?php

namespace Itomych\Pwinty\Api\Objects;

use Itomych\Pwinty\Api\AbstractBaseObject;
use stdClass;

class Shipment extends AbstractBaseObject
{

    public const STATUS_IN_PROGRESS = 'InProgress';
    public const STATUS_SHIPPED = 'Shipped';

    /**
     * @var string
     */
    protected $carrier;

    /**
     * @var array[int]
     */
    protected $photoIds;

    /**
     * @var string
     */
    protected $shipmentId;

    /**
     * @var string
     */
    protected $trackingNumber;

    /**
     * @var string
     */
    protected $trackingUrl;

    /**
     * @var bool
     */
    protected $isTracked;

    /**
     * @var string
     */
    protected $earliestEstimatedArrivalDate;

    /**
     * @var string
     */
    protected $latestEstimatedArrivalDate;

    /**
     * @var string
     */
    protected $shippedOn;

    /**
     * @param stdClass $data
     */
    public function setData(stdClass $data)
    {
        $this->carrier = $data->carrier;
        $this->photoIds = $data->photoIds;
        $this->shipmentId = $data->shipmentId;
        $this->trackingNumber = $data->trackingNumber;
        $this->trackingUrl = $data->trackingUrl;
        $this->isTracked = $data->isTracked;
        $this->earliestEstimatedArrivalDate = $data->earliestEstimatedArrivalDate;
        $this->latestEstimatedArrivalDate = $data->latestEstimatedArrivalDate;
        $this->shippedOn = $data->shippedOn;
    }

    /**
     * @return string
     */
    public function getCarrier(): string
    {
        return $this->carrier;
    }

    /**
     * @return array
     */
    public function getPhotoIds(): array
    {
        return $this->photoIds;
    }

    /**
     * @return string
     */
    public function getShipmentId(): string
    {
        return $this->shipmentId;
    }

    /**
     * @return string
     */
    public function getTrackingNumber(): string
    {
        return $this->trackingNumber;
    }

    /**
     * @return string
     */
    public function getTrackingUrl(): string
    {
        return $this->trackingUrl;
    }

    /**
     * @return bool
     */
    public function isTracked(): bool
    {
        return $this->isTracked;
    }

    /**
     * @return string
     */
    public function getEarliestEstimatedArrivalDate(): string
    {
        return $this->earliestEstimatedArrivalDate;
    }

    /**
     * @return string
     */
    public function getLatestEstimatedArrivalDate(): string
    {
        return $this->latestEstimatedArrivalDate;
    }

    /**
     * @return string
     */
    public function getShippedOn(): string
    {
        return $this->shippedOn;
    }



}