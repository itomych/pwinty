<?php

namespace Itomych\Pwinty\Api\Objects;

use Itomych\Pwinty\Api\AbstractBaseObject;
use Doctrine\Common\Collections\ArrayCollection;
use Itomych\Pwinty\Api\Objects\Shipment;
use stdClass;

class ShippingInfo extends AbstractBaseObject
{
    /**
     * @var float
     */
    protected $price;

    /**
     * @var ArrayCollection|Shipment[]
     */
    protected $shipments;

    /**
     * Country constructor.
     * @param stdClass $data
     */
    public function setData(stdClass $data)
    {
        $this->price = $data->price / 100;
        $this->shipments = $this->objectManager->mapToClass($data->shipments, Shipment::class);
    }

    /**
     * @return Shipment[]|ArrayCollection
     */
    public function getShipments(): ArrayCollection
    {
        return $this->shipments;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }
}