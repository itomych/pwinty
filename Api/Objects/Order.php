<?php

namespace Itomych\Pwinty\Api\Objects;

use Itomych\Pwinty\Api\AbstractBaseObject;
use Doctrine\Common\Collections\ArrayCollection;
use stdClass;

class Order extends AbstractBaseObject
{

    /**
     * Shipment types
     */
    public const SHIP_BUDGET = 'Budget';
    public const SHIP_STANDARD = 'Standard';
    public const SHIP_EXPRESS = 'Express';
    public const SHIP_OVERNIGHT = 'Overnight';

    /**
     * Payment types
     */
    public const PAY_INVOICE_ME = 'InvoiceMe';
    public const PAY_INVOICE_RECIPIENT = 'InvoiceRecipient';

    /**
     * Order possible statuses
     */
    public const STATUS_NOT_YET_SUBMITTED = 'NotYetSubmitted';
    public const STATUS_SUBMITTED = 'Submitted';
    public const STATUS_AWAITING_PAYMENT = 'AwaitingPayment';
    public const STATUS_COMPLETE = 'Complete';
    public const STATUS_CANCELLED = 'Cancelled';

    /**
     * Optional* = Although these are optional for order creation, they are required when submitting your order.
     */

    /**
     * @var string
     */
    public $recipientName;

    /**
     * @var string
     */
    public $address1;

    /**
     * @var string (optional*)
     */
    public $addressTownOrCity;

    /**
     * @var string (optional*)
     */
    public $stateOrCounty;

    /**
     * @var string (optional*)
     */
    public $postalOrZipCode;

    /**
     * @var string
     */
    public $countryCode;

    /**
     * @var string (see ship constants)
     */
    public $preferredShippingMethod;


    /**
     * @var string (optional)
     */
    public $merchantOrderId;

    /**
     * @var string (optional)
     */
    public $address2;

    /**
     * @var string (optional, see to payment constants)
     */
    public $payment = self::PAY_INVOICE_ME;

    /**
     * @var string (optional) Customer's mobile number for shipping updates and courier contact.
     */
    public $mobileTelephone;

    /**
     * @var string (optional) Customer's non-mobile phone number for shipping updates and courier contact.
     */
    public $telephone;

    /**
     * @var string (optional)
     */
    public $email;

    /**
     * @var string (optional) Used for orders where an invoice amount must be supplied (e.g. to Middle East).
     */
    public $invoiceAmountNet;

    /**
     * @var string (optional) Used for orders where an invoice amount must be supplied (e.g. to Middle East).
     */
    public $invoiceTax;

    /**
     * @var string (optional) Used for orders where an invoice amount must be supplied (e.g. to Middle East).
     */
    public $invoiceCurrency;


    /**
     * @var int
     */
    protected $id;

    /**
     * @var string (see statuses constants)
     */
    protected $status;

    /**
     * @var string - If payment is set to InvoiceRecipient then the URL the customer should be sent to complete payment.
     */
    protected $paymentUrl;

    /**
     * @var float - How much Pwinty will charge you for this order.
     */
    protected $price;

    /**
     * @var ShippingInfo
     */
    protected $shippingInfo;

    /**
     * @var Image[]|ArrayCollection
     */
    protected $images;

    /**
     * @var string
     */
    protected $created;

    /**
     * @var string
     */
    protected $lastUpdated;

    /**
     * @var bool
     */
    protected $canCancel;

    /**
     * @var bool
     */
    protected $canHold;

    /**
     * @var bool
     */
    protected $canUpdateShipping;

    /**
     * @var bool
     */
    protected $canUpdateImages;


    /**
     * @param stdClass $data
     */
    public function setData(stdClass $data)
    {

        $this->id = $data->id;
        $this->address1 = $data->address1;
        $this->address2 = $data->address2;
        $this->postalOrZipCode = $data->postalOrZipCode;
        $this->countryCode = $data->countryCode;
        $this->addressTownOrCity = $data->addressTownOrCity;
        $this->recipientName = $data->recipientName;
        $this->stateOrCounty = $data->stateOrCounty;
        $this->status = $data->status;
        $this->paymentUrl = $data->paymentUrl;
        $this->price = $data->price / 100;
        $this->shippingInfo = $this->objectManager->toClass($data->shippingInfo, ShippingInfo::class);
        $this->images = $this->objectManager->mapToClass($data->images, Image::class);
        $this->merchantOrderId = $data->merchantOrderId;
        $this->preferredShippingMethod = $data->preferredShippingMethod;
        $this->mobileTelephone = $data->mobileTelephone;
        $this->lastUpdated = $data->lastUpdated;
        $this->canCancel = $data->canCancel;
        $this->canHold = $data->canHold;
        $this->canUpdateShipping = $data->canUpdateShipping;
        $this->canUpdateImages = $data->canUpdateImages;
        $this->invoiceAmountNet = $data->invoiceAmountNet;
        $this->invoiceTax = $data->invoiceTax;
        $this->invoiceCurrency = $data->invoiceCurrency;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @return string
     */
    public function getPaymentUrl(): string
    {
        return $this->paymentUrl;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @return ShippingInfo
     */
    public function getShippingInfo(): ShippingInfo
    {
        return $this->shippingInfo;
    }

    /**
     * @return Image[]|ArrayCollection
     */
    public function getImages(): ArrayCollection
    {
        return $this->images;
    }

    /**
     * @return string
     */
    public function getCreated(): string
    {
        return $this->created;
    }

    /**
     * @return string
     */
    public function getLastUpdated(): string
    {
        return $this->lastUpdated;
    }

    /**
     * @return bool
     */
    public function isCanCancel(): bool
    {
        return $this->canCancel;
    }

    /**
     * @return bool
     */
    public function isCanHold(): bool
    {
        return $this->canHold;
    }

    /**
     * @return bool
     */
    public function isCanUpdateShipping(): bool
    {
        return $this->canUpdateShipping;
    }

    /**
     * @return bool
     */
    public function isCanUpdateImages(): bool
    {
        return $this->canUpdateImages;
    }

    /**
     * @param string $status
     * @throws \Exception
     */
    public function setStatus(string $status): void
    {
        if (!empty($this->id)) {
            $this->status = $status;
        } else {
            throw new \Exception('Don\'t can set status of not saved order');
        }
    }
}