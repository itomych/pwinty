<?php

namespace Itomych\Pwinty\Api\Objects;

use Itomych\Pwinty\Api\AbstractBaseObject;
use stdClass;

class Product extends AbstractBaseObject
{
    /**
     * @var string
     */
    public $sku;

    /**
     * @var int
     */
    public $price;

    /**
     * @var string
     */
    public $currency;

    /**
     * @param stdClass $data
     */
    public function setData(stdClass $data)
    {
        $this->sku = $data->sku;
        $this->price = $data->price;
        $this->currency = $data->currency;
    }
}