<?php

namespace Itomych\Pwinty\Api\Objects;

use Itomych\Pwinty\Api\AbstractBaseObject;
use stdClass;

class Country extends AbstractBaseObject
{
    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $isoCode;

    /**
     * @param stdClass $data
     */
    public function setData(stdClass $data)
    {
        $this->name = $data->name;
        $this->isoCode = $data->isoCode;
    }
}