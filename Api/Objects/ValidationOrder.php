<?php

namespace Itomych\Pwinty\Api\Objects;

use Itomych\Pwinty\Api\AbstractBaseObject;
use Doctrine\Common\Collections\ArrayCollection;
use Itomych\Pwinty\Api\Objects\ValidationImage;
use stdClass;

class ValidationOrder extends AbstractBaseObject
{
    /**
     * You cannot submit any more orders until you have paid off the balance outstanding on your account.
     */
    public const ERROR_ACCOUNT_BALANCE_INSUFFICIENT = 'AccountBalanceInsufficient';

    /**
     * One or more of the images in the order has errors- see the photos object for more information.
     */
    public const ERROR_ITEMS_CONTAINING_ERRORS = 'ItemsContainingErrors';

    /**
     * The order has no images associated with it, so cannot be submitted.
     */
    public const ERROR_NO_ITEMS_IN_ORDER = 'NoItemsInOrder';

    /**
     * The recipient address fields on the order were not properly set. You must supply at least address1, addressTownOrCity, postalOrZipCode and countryCode.
     */
    public const ERROR_POSTAL_ADDRESS_NOT_SET = 'PostalAddressNotSet';

    /**
     * Default text for validation messages
     * @var array
     */
    protected $errorMessages = [
        'AccountBalanceInsufficient' => 'You cannot submit any more orders until you have paid off the balance outstanding on your account.',
        'ItemsContainingErrors' => 'One or more of the images in the order has errors- see the photos object for more information.',
        'NoItemsInOrder' => 'The order has no images associated with it, so cannot be submitted.',
        'PostalAddressNotSet' => 'The recipient address fields on the order were not properly set. You must supply at least address1, addressTownOrCity, postalOrZipCode and countryCode'
    ];

    /**
     * @var int
     */
    protected $id;

    /**
     * @var bool
     */
    protected $isValid;

    /**
     * @var array - of errors (see errors types constants)
     */
    protected $generalErrors;

    /**
     * @var ArrayCollection|ValidationImage[]
     */
    protected $photos;

    /**
     * Country constructor.
     * @param stdClass $data
     */
    public function setData(stdClass $data): void
    {
        $this->id = $data->id;
        $this->isValid = $data->isValid;
        $this->generalErrors = $data->generalErrors;
        $this->photos = $this->objectManager->mapToClass($data->photos, ValidationImage::class);
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return bool
     */
    public function isValid(): bool
    {
        return $this->isValid;
    }

    /**
     * @return array
     */
    public function getGeneralErrors(): array
    {
        return $this->generalErrors;
    }

    /**
     * @return ValidationImage[]|ArrayCollection
     */
    public function getPhotos(): ArrayCollection
    {
        return $this->photos;
    }

    /**
     * @return ArrayCollection
     */
    public function getErrorMessages(): ArrayCollection
    {
        $messages = new ArrayCollection();
        foreach ($this->generalErrors as $error) {
            $messages->set($error, $this->errorMessages[$error]);
        }

        $messages->set('photos', []);
        foreach ($this->photos as $validationImage) {
            $imageMessages = $validationImage->getErrorMessages();
            $imageId = $validationImage->getId();

            $messages['photos'][$imageId] = $imageMessages;
        }

        return $messages;
    }


}