<?php

namespace Itomych\Pwinty\Api\Objects;

use Itomych\Pwinty\Api\AbstractBaseObject;
use stdClass;

class Image extends AbstractBaseObject
{
    /**
     * Images sizing types
     */

    /**
     * The image you upload will be cropped until it exactly fits the aspect ratio (height divided by width)
     * of the printing area of the product you chose.
     */
    public const RESIZING_CROP = 'Crop';

    /**
     * The image you upload will be shrunk until all the image fits on the printing area of the product.
     * This can lead to white bars at the edges. Think of a widescreen movie on an old 4:3 TV.
     */
    public const RESIZING_SHRINK_TO_FIT = 'ShrinkToFit';

    /**
     * The image you upload will be resized until all of the image exactly fits all of the on the printing
     * area of the product. This means that if the aspect ratio of the image and the printing area do not match,
     * the image will be stretched or squashed to fit the size.
     */
    public const RESIZING_SHRINK_TO_EXACT_FIT = 'ShrinkToExactFit';

    /**
     * Image statuses
     */

    /**
     * There are two ways of uploading a file to Pwinty. You can either specify a url or you can POST
     * using a multi-part upload. If you see this status, it means you have yet to do either for this image.
     */
    public const STATUS_AWAITING_URL_OR_DATA = 'AwaitingUrlOrData';

    /**
     * You have specified a url associated with the image, but Pwinty hasn't yet downloaded it.
     * There's nothing you need to do about this.
     */
    public const STATUS_NOT_YET_DOWNLOADED = 'NotYetDownloaded';

    /**
     * We've received your image and verified it is a valid file format. All is ready to go.
     */
    public const STATUS_OK = 'Ok';

    /**
     * We tried using the url you specified to grab the image, but we didn't find it there.
     */
    public const STATUS_FILE_NOT_FOUND_AT_URL = 'FileNotFoundAtUrl';

    /**
     * You uploaded a file, but it wasn't in a valid format, or we checked the url you specified
     * and didn't find a valid image there.
     */
    public const STATUS_INVALID = 'Invalid';


    /**
     *
     * orderId    The ID of the order (in URL).
     * sku    An identification code of the product for this image.
     * url    The image's URL.
     * copies    Number of copies of the image to include in the order.
     * sizing    How the image should be resized when printing.
     * priceToUser optional    If payment is set to InvoiceRecipient then the price (in cents/pence) you want to charge for each copy. Only available if your payment option is InvoiceRecipient.
     * md5Hash optional    An MD5 hash of the image file.
     * attributes optional    An object with properties representing the attributes for the image.
     */

    /**
     * @var string
     */
    public $sku;

    /**
     * @var string - If image is to be downloaded by Pwinty, the image's URL.
     */
    public $url;

    /**
     * @var int - Number of copies of the image to include in the order.
     */
    public $copies;

    /**
     * @var string - How the image should be resized when printing. (See sizing constants)
     */
    public $sizing;

    /**
     * (optional)
     * If payment is set to InvoiceRecipient then the price (in cents/pence) you want to charge
     * for each copy. Only available if your payment option is InvoiceRecipient.
     * @var int
     */
    public $priceToUser;

    /**
     * (optional)
     * @var string
     */
    public $md5Hash;

    /**
     * (optional)
     * @var array - An object containing all the attributes set on the object.
     */
    public $attributes;

    /**
     * @var int
     */
    protected $id;

    /**
     * @var string - Current status of the image. (See statuses in constants)
     */
    protected $status;

    /**
     * @var float
     */
    protected $price;

    /**
     * @var string - A URL to image after cropping.
     */
    protected $previewUrl;

    /**
     * @var string - A URL that will serve up a thumbnail of the image after cropping.
     */
    protected $thumbnailUrl;

    /**
     * Image constructor.
     * @param stdClass $data
     */
    public function setData(stdClass $data)
    {
        $this->id = $data->id;
        $this->sku = $data->sku;
        $this->url = $data->url;
        $this->status = $data->status;
        $this->copies = $data->copies;
        $this->sizing = $data->sizing;
        $this->priceToUser = $data->priceToUser;
        $this->price = $data->price;
        $this->md5Hash = $data->md5Hash;
        $this->previewUrl = $data->previewUrl;
        $this->thumbnailUrl = $data->thumbnailUrl;
        $this->attributes = $data->attributes;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @return string
     */
    public function getPreviewUrl(): string
    {
        return $this->previewUrl;
    }

    /**
     * @return string
     */
    public function getThumbnailUrl(): string
    {
        return $this->thumbnailUrl;
    }

}