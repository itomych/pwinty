<?php

namespace Itomych\Pwinty\Api\Objects;

use Itomych\Pwinty\Api\AbstractBaseObject;
use Doctrine\Common\Collections\ArrayCollection;
use stdClass;

class ValidationImage extends AbstractBaseObject
{

    /**
     * Errors
     */

    /**
     * We could not download an image from the supplied URL, after multiple attempts.
     */
    public const ERROR_FILE_COULD_NOT_BE_DOWNLOADED = 'FileCouldNotBeDownloaded';

    /**
     * You haven't submitted an image URL nor have you POSTed an image.
     */
    public const ERROR_NO_IMAGE_FILE = 'NoImageFile';

    /**
     * Image file format is not valid.
     */
    public const ERROR_INVALID_IMAGE_FILE = 'InvalidImageFile';

    /**
     * Number of copies of the image has been set to zero.
     */
    public const ERROR_ZERO_COPIES = 'ZeroCopies';

    /**
     * Default text for validation messages
     * @var array
     */
    protected $errorMessages = [
        'FileCouldNotBeDownloaded' => 'We could not download an image from the supplied URL, after multiple attempts.',
        'NoImageFile' => 'You haven\'t submitted an image URL nor have you POSTed an image.',
        'InvalidImageFile' => 'Image file format is not valid.',
        'ZeroCopies' => 'Number of copies of the image has been set to zero.'
    ];

    /**
     * Warnings
     */

    /**
     * The image supplied does not match the aspect ratio of the printing area of the product. We will need to crop or resize it.
     */
    public const WARNING_CROPPING_WILL_OCCUR = 'CroppingWillOccur';

    /**
     * The image supplied is below the recommended resolution.
     */
    public const WARNING_PICTURE_SIZE_TO_SMALL = 'PictureSizeTooSmall';

    /**
     * You've supplied an image with a URL, but we haven't downloaded it yet. This means we can't check the image size at the moment.
     */
    public const WARNING_COULD_NOT_VALIDATE_IMAGE_SIZE = 'CouldNotValidateImageSize';

    /**
     * You've supplied an image with a URL, but we haven't downloaded it yet. This means we can't check the aspect ratio at the moment.
     */
    public const WARNING_COULD_NOT_VALIDATE_ASPECT_RATIO = 'CouldNotValidateAspectRatio';

    /**
     * One of the product attributes set on the image is invalid.
     */
    public const WARNING_ATTRIBUTE_NOT_VALID = 'AttributeNotValid';

    /**
     * @var int
     */
    protected $id;

    /**
     * @var array - of errors (see errors constants)
     */
    protected $errors;

    /**
     * @var array - of errors (see warnings constants)
     */
    protected $warnings;

    /**
     * Country constructor.
     * @param stdClass $data
     */
    public function setData(stdClass $data)
    {
        $this->id = $data->id;
        $this->errors = $data->errors;
        $this->warnings = $data->warnings;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return array
     */
    public function getErrors(): array
    {
        return $this->errors;
    }

    /**
     * @return array
     */
    public function getWarnings(): array
    {
        return $this->warnings;
    }

    /**
     * @return ArrayCollection
     */
    public function getErrorMessages(): ArrayCollection
    {
        $messages = new ArrayCollection();
        foreach ($this->errors as $error) {
            $messages->set($error, $this->errorMessages[$error]);
        }

        return $messages;
    }

}