<?php

namespace Itomych\Pwinty\Api\Service;

use Itomych\Pwinty\Api\AbstractBaseObject;
use Itomych\Pwinty\Api\AbstractService;
use Itomych\Pwinty\Api\Objects\Image;
use Doctrine\Common\Collections\ArrayCollection;

class ImageService extends AbstractService
{
    /**
     * @var string
     */
    protected $addImageUrl = 'orders/{id}/images';

    /**
     * @var string
     */
    protected $addMultipleUrl = 'orders/{id}/images/batch';

    /**
     * @param string $sku
     * @param string $url
     * @param int $copies
     * @param string $sizing
     * @return Image
     */
    public function create(string $sku, string $url, int $copies = 1, string $sizing = Image::RESIZING_CROP): Image
    {
        /**
         * @var $image Image
         */
        $image = $this->objectManager->createEmptyObject(Image::class);
        $image->sku = $sku;
        $image->url = $url;
        $image->copies = $copies;
        $image->sizing = $sizing;

        return $image;
    }

    /**
     * @param int $orderId
     * @param Image $image
     * @return Image|AbstractBaseObject
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function addToOrder(int $orderId, Image $image): Image
    {
        $url = $this->buildUrl($this->addImageUrl, ['id' => $orderId]);

        $response = $this->connection->post($url, (array)$image);

        return $this->objectManager->toClass($response->data, Image::class);
    }

    /**
     * @param int $orderId
     * @param array $images
     * @return ArrayCollection|Image[]
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function addToOrderMultiple(int $orderId, array $images): ArrayCollection
    {
        $url = $this->buildUrl($this->addMultipleUrl, ['id' => $orderId]);

        $response = $this->connection->post($url, $images);

        return $this->objectManager->mapToClass($response->items, Image::class);
    }
}