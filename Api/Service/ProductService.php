<?php

namespace Itomych\Pwinty\Api\Service;

use Itomych\Pwinty\Api\AbstractBaseObject;
use Itomych\Pwinty\Api\AbstractService;
use Itomych\Pwinty\Api\Objects\Product;
use Doctrine\Common\Collections\ArrayCollection;

class ProductService extends AbstractService
{
    /**
     * @var string
     */
    protected $pricesUrl = 'catalogue/prodigi%20direct/destination/any/prices';

    /**
     * @param string $sku
     * @return Product|AbstractBaseObject
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function get(string $sku): Product
    {
        $response = $this->connection->post($this->pricesUrl, ['skus' => [$sku]]);

        return $this->objectManager->toClass($response->prices[0], Product::class);
    }

    /**
     * @param array $skus
     * @return ArrayCollection|Product[]
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function list(array $skus): ArrayCollection
    {
        $response = $this->connection->post($this->pricesUrl, ['skus' => $skus]);

        return $this->objectManager->mapToClass($response->prices, Product::class);
    }
}