<?php

namespace Itomych\Pwinty\Api\Service;

use Itomych\Pwinty\Api\AbstractService;
use Itomych\Pwinty\Api\Objects\Country;
use Doctrine\Common\Collections\ArrayCollection;

class CountryService extends AbstractService
{
    /**
     * @var string
     */
    protected $countriesUrl = 'countries';

    /**
     * @return ArrayCollection|Country[]
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function list(): ArrayCollection
    {
        $response = $this->connection->get($this->countriesUrl);

        return $this->objectManager->mapToClass($response->data, Country::class);
    }
}