<?php

namespace Itomych\Pwinty\Api\Service;

use Itomych\Pwinty\Api\AbstractBaseObject;
use Itomych\Pwinty\Api\AbstractService;
use Itomych\Pwinty\Api\Objects\Order;
use Itomych\Pwinty\Api\Objects\ValidationOrder;
use Doctrine\Common\Collections\ArrayCollection;
use stdClass;

class OrderService extends AbstractService
{

    /**
     * Available order statuses for change status
     */
    public const ORDER_STATUS_CANCELLED = 'Cancelled';

    public const ORDER_STATUS_AWAITING_PAYMENT = 'AwaitingPayment';

    public const ORDER_STATUS_SUBMITTED = 'Submitted';

    /**
     * @var string
     */
    protected $ordersUrl = 'orders';

    /**
     * @var string
     */
    protected $orderUrl = 'orders/{id}';

    /**
     * @var string
     */
    protected $validateOrderUrl = 'orders/{id}/SubmissionStatus';

    /**
     * @var string
     */
    protected $changeStatusUrl = 'orders/{id}/status';

    /**
     * @param int $limit
     * @param int $offset
     * @return ArrayCollection|Order[]
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function list(int $limit = 100, int $offset = 0): ArrayCollection
    {
        $response = $this->connection->get($this->ordersUrl, ['limit' => $limit, 'offset' => $offset]);

        return $this->objectManager->mapToClass($response->data->content, Order::class);
    }

    /**
     * @param Order $order
     * @return Order|AbstractBaseObject
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function update(Order $order): Order
    {
        $url = $this->buildUrl($this->orderUrl, ['id' => $order->getId()]);

        $response = $this->connection->post($url, (array)$order);

        return $this->objectManager->toClass($response->data, Order::class);
    }

    /**
     * @param array $data
     * @return Order
     */
    public function create(array $data): Order
    {
        /**
         * @var $order Order
         */
        $order = $this->objectManager->createEmptyObject(Order::class);
        $order->stateOrCounty = $data['country_name'];
        $order->address1 = $data['address1'];
        $order->addressTownOrCity = $data['city'];
        $order->countryCode = $data['country_code'];
        $order->recipientName = $data['username'];
        $order->postalOrZipCode = $data['zip'];

        $order->email = $data['email'] ?? '';
        $order->address2 = $data['address2'] ?? '';
        $order->merchantOrderId = $data['merchant_order_id'] ?? '';
        $order->mobileTelephone = $data['mobile_telephone'] ?? '';
        $order->payment = $data['payment'] ?? '';
        $order->preferredShippingMethod = $data['preferred_shipping_method'] ?? '';
        $order->telephone = $data['telephone'] ?? '';
        $order->invoiceAmountNet = $data['invoice_amount_net'] ?? '';
        $order->invoiceCurrency = $data['invoice_currency'] ?? '';
        $order->invoiceTax = $data['invoice_tax'] ?? '';

        return $order;
    }

    /**
     * @param Order $order
     * @return Order|AbstractBaseObject
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function save(Order $order): Order
    {
        $response = $this->connection->post($this->ordersUrl, (array)$order);

        return $this->objectManager->toClass($response->data, Order::class);
    }

    /**
     * @param int $id
     * @return Order|AbstractBaseObject
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function get(int $id): Order
    {
        $url = $this->buildUrl($this->orderUrl, ['id' => $id]);

        $response = $this->connection->get($url);

        return $this->objectManager->toClass($response->data, Order::class);
    }

    /**
     * @param int $orderId
     * @return ValidationOrder|AbstractBaseObject
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function validate(int $orderId): ValidationOrder
    {
        $url = $this->buildUrl($this->validateOrderUrl, ['id' => $orderId]);

        $response = $this->connection->get($url);

        return $this->objectManager->toClass($response->data, ValidationOrder::class);
    }

    /**
     * @param int $orderId
     * @return stdClass
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function submit(int $orderId): stdClass
    {
        $url = $this->buildUrl($this->changeStatusUrl, ['id' => $orderId]);

        return $this->connection->post($url, ['status' => self::ORDER_STATUS_SUBMITTED]);
    }

    /**
     * @param int $orderId
     * @return stdClass
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function cancel(int $orderId): stdClass
    {
        $url = $this->buildUrl($this->changeStatusUrl, ['id' => $orderId]);

        return $this->connection->post($url, ['status' => self::ORDER_STATUS_CANCELLED]);
    }

    /**
     * Set status to AwaitingPayment
     * @param int $orderId
     * @return stdClass
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function awaitPayment(int $orderId): stdClass
    {
        $url = $this->buildUrl($this->changeStatusUrl, ['id' => $orderId]);

        return $this->connection->post($url, ['status' => self::ORDER_STATUS_AWAITING_PAYMENT]);
    }
}
