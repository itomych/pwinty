<?php

namespace Itomych\Pwinty;

use Itomych\Pwinty\DependencyInjection\Compiler\DefinitionObjectsPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class ItomychPwintyBundle extends Bundle
{

    public function build(ContainerBuilder $container)
    {
    }

}