PHP Symfony Bundle Pwinty Api v3.0
==================================

Description
-----------

This the symfony bundle for pwinty api v3.0 

Installation
-------------

```
composer require itomych/pwinty
```

Add to **.env** file 

```.env
PWINTY_SANDBOX_MODE=true
PWINTY_MERCHANT_ID=
PWINTY_API_KEY=
``` 

Usage
-----

```php
<?php

namespace App\Api\Controller;

use Itomych\Pwinty\Api\PwintyApi;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class ExampleController extends AbstractController
{
    /**
     * @Route("/example", name="example", methods={"POST"})
     * @param $request Request 
     * @param $pwinty PwintyApi
     * @return JsonResponse
     */
    public function index(Request $request, PwintyApi $pwinty): JsonResponse {
        
        //Creating order case
        //Please see all required params in \Itomych\Pwinty\Api\Objects\Order
        $order = $pwinty->order->create($request->request->all());
        $order = $pwinty->order->save($order);
        
        $image = $pwinty->image->create($request->get('sku'), $request->get('image_url'));
        $pwinty->image->addToOrder($order->getId(), $image);
        //or you can add multiple images
        // $pwinty->image->addToOrderMultiple($order->getId(), $imagesArray);
        
        $validateResult = $pwinty->order->validate($order->getId());
        if($validateResult->isValid()){
            $pwinty->order->submit($order->getId());
            $order = $pwinty->order->get($order->getId());
            
//            return $this->json(json_encode($order));
        } else {
            $pwinty->order->cancel($order->getId());
            
//            return $this->json(print_r($validateResult->getErrorMessages(), 1), JsonResponse::HTTP_BAD_REQUEST);
        }
        
        //list orders
        $limit = 100;
        $offset = 0;
        $ordersList = $pwinty->order->list($limit, $offset);
        
        //list countries
        $countriesList = $pwinty->country->list();
        
        //product pricing
        $skusArray = ['P-FIN-CPP-297X419', 'P-FIN-CPP-297X210'];
        $listProducts = $pwinty->product->list($skusArray);
        $oneProduct = $pwinty->product->get('P-FIN-CPP-297X419');
        
        return $this->json('');
    }
    
}

```
